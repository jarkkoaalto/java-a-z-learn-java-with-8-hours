package section09;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;

public class ReadFileDemo {


		static Scanner reader = new Scanner(System.in);
		
		public static void main(String[] args) {
			fileReader();	
		}
		
		
	static void fileReader() {
		try {
			FileReader r = new FileReader("test.txt");
			int c; 
			while((c = r.read()) != -1) {
				System.out.print((char)c);
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		
	}

	static void fileWriter() {
		FileWriter f;
		try {
			f = new FileWriter("test.txt");
			f.write("We are going to become a JAVA programmer");
			f.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Done");
	
	}
}
