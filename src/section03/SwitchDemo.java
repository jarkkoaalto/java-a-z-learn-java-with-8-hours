package section03;

import java.util.Scanner;

public class SwitchDemo {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		
		
		System.out.println("Enter the number (1-4) ");
		int number = reader.nextInt();
		
		switch(number) {
		case 1: 
			System.out.println("You entered number 1");
			break;
		case 2:
			System.out.println("You entered number 2");
			break;
		case 3:
			System.out.println("You entered number 3");
			break;
		case 4: 
			System.out.println("You entered number 4");
			break;
		default:
			System.out.println("You entered unknown number");
			break;
		}
		
	}

}
