package section05;

public class BreakAndContinueDemo {

	public static void main(String[] args) {
		
		/*
		 * for(int i=0;i<100;i++) {
		 
			
			if(i % 4 == 0)
			{
				continue;
			}
			System.out.println(i);
			
		}
		*/
		System.out.println();
		
		for(int i = 0; i<100;i++) {
		if(i % 4 == 0) {
			break;
		}
		
		System.out.println(i);
		}
	}

}
