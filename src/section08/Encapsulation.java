package section08;

public class Encapsulation {
	
	
	private String Name = "Sachin";
	private int roll;
	
	public Encapsulation(int roll) {
		this.roll = roll;
	}

	public void getRoll() {
		System.out.println(roll);
	}

	public void setRoll(int roll) {
		this.roll = roll;
	}
	
	

	

}
