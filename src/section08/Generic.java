package section08;

public class Generic<M,N> {
	public static void main(String [] args) {
		
		String[] a1 = {"Sunday", "Monday", "Tuesday"};
		Integer []a2 = {1,5,7,8,44};
		Double [] a3 = {2.4,6.3,6.8};
		
		print(a1);
		print(a2);
		print(a3);
		
	}
	
	static <T> void print(T [] ar) {
		for(T s: ar) {
			System.out.println(s);
		}
	}
	
}
