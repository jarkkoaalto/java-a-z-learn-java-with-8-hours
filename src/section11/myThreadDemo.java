package section11;

public class myThreadDemo extends Thread {

	String name;
	public myThreadDemo(String name) {
		this.name = name;
	}
	public static void main(String[] args)  throws InterruptedException{
		myThreadDemo t = new myThreadDemo("t1");
		t.start();
		
		myThreadDemo t2 = new myThreadDemo("t2");
		t2.start();
		Thread.sleep(5000);
		
		

	}
	
	@Override
	public void run() {
		show();
	}
	
	void show() {
		int x = 0;
		while(x < 10) {
			System.out.println(name+ " " + x);
			x++;
		}
	}
}
