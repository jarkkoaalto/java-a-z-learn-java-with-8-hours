package section06;

import java.util.Scanner;

public class TwoDimensionalArray {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		int[][] info = new int[3][3]; 
		
		info[0][0] = 2;
		info[0][1] = 4;
		info[0][2] = 6;
		info[1][0] = 8;
		info[1][1] = 10;
		info[1][2] = 12;
		info[2][0] = 14;
		info[2][1] = 16;
		info[2][2] = 18;
		
		for(int i = 0; i<3; i++) {
			for(int j=0; j<3;j++) {
				//System.out.print(info[i][j] + "\t");
				
				if(i == j) {
					System.out.println(info[i][j] + "\t");
				}
			}
			//System.out.println();
		}

	}

}
