package section14H;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	public static void main(String [] args) throws Exception {
		ServerSocket sersock = new ServerSocket(3000);
		System.out.println("Server is ready");
		
		Socket sock = sersock.accept();
		
		OutputStream ostream = sock.getOutputStream();
		PrintWriter pwrite = new PrintWriter(ostream, true);
		
		InputStream istream = sock.getInputStream();
		BufferedReader receiveRead = new BufferedReader(new InputStreamReader(istream));
		String val;
		val = receiveRead.readLine();
		if(val != null) {
			System.out.println(val);
		}
		
		pwrite.println(val);
		
		System.out.flush();
	}

}
