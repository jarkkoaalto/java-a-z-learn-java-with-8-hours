package section09;

import java.util.Scanner;

public class ExceptionDemo {
	
	static Scanner reader = new Scanner(System.in);
	public static void main(String [] args) {
		
		try {
		int a = reader.nextInt() ;
		System.out.println(a);
		}catch(Exception e) {
			System.out.println(e.getMessage());
			
		}finally {
		System.out.println("Continue working ! ");
		}
	}

}
