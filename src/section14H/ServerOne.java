
package section14H;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.net.ServerSocket;

import java.net.*;
import java.io.*;

/**
 *
 * @author sachin
 */
public  class ServerOne {
  
    
    public static void main(String[] args) throws Exception{
        
        
        
        ServerSocket sersock = new ServerSocket(3000);
        
        System.out.println("Server is ready");
        
        Socket sock = sersock.accept();
        
        OutputStream ostream = sock.getOutputStream();
        
        PrintWriter pwrite = new PrintWriter(ostream,true);
        
        InputStream istream = sock.getInputStream();
        
        BufferedReader receiveRead = new BufferedReader(new InputStreamReader(istream));
        
        String v1,v2,op;
        v1 = receiveRead.readLine();
        v2 = receiveRead.readLine();
        
        op = receiveRead.readLine();
        
        int a = Integer.parseInt(v1);
        int b = Integer.parseInt(v2);
        
        int result = 0;
        
        if(op != null) {
        	if(op.compareTo("+") == 0) {
        		result = a+b;
        	}
        	if(op.compareTo("-") == 0) {
        		result = a-b;
        	}
        	if(op.compareTo("*") == 0) {
        		result = a*b;
        	}
        	
        }
        
        pwrite.println(result);
        
        System.out.flush();
        
         
    
}

    
}


