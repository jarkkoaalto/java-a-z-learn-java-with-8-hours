package section03;

public class LogicDemo {
	public static void main(String [] args) {
		System.out.println(true);
		System.out.println("2 > 6 is " + (2 > 6));
		System.out.println("2 <= 6 is " + (2 <= 6));
		System.out.println("6 == 6 is " + (6 == 6));
		System.out.println("6 != 6 is " + (6 != 6));
		
		// and && () && is true
		// or() or() is anyone true then code will run
		System.out.println("(2<8) && (9<9) is " + ((2<8) && (9<9)));
		System.out.println("(2<8) || (9<9) is " + ((2<8) || (9<9)));
		System.out.println((!(9<0)));
	}

}
