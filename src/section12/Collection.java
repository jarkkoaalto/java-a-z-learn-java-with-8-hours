package section12;

import java.util.ArrayList;

public class Collection {

	public static void main(String[] args) {
		ArrayList<String> ar = new ArrayList<String>();
		
		ar.add("John");
		ar.add("Matt");
		ar.add("Rock");
		
		System.out.println(ar.get(0));
		ar.remove(0);
		System.out.println(ar);

		ArrayList<employee> em = new ArrayList<employee>();
		
		
		em.add(new employee("Sachin", 20));
		em.add(new employee("Sarah", 21));
		em.add(new employee("Ben", 22));
		
		for(employee st:em) {
			System.out.println(st.name);
			System.out.println(st.age);
		}
		
	}
	static public class employee{
		
		String name;
		int age;
		public employee(String name, int age) {
			this.name = name;
			this.age = age;
		}
	}

}
