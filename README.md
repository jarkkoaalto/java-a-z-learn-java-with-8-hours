# java-A-Z-Learn-java-with-8-hours

About this course
Never underestimate power of Java, Make real world projects by just enrolling into this course!!

### Section 01: Introduction and setup for JAVA!!
### Section 02: More Than Basics!!
### Section 03: Operations & Priorities
### Section 05: Getting Loopy Loops!!
### Section 06: Dealing with arrays!!
### Section 07: User Defined Functions!!
### Section 08: Object Oriented Programming
### Section 09: File Handling
### Section 10: Databases: using sqlite !!
### Section 11: Multi-threading
### Section 12: Collections
### Section 13: Working with GUI
### Section 14: Network Programming!!
    