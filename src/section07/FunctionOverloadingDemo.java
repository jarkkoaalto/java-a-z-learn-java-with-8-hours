package section07;

public class FunctionOverloadingDemo {
	
	public static void main(String [] args) {
		Area(2,4);
		Area(13);
		
	}
	
	 static void Area (int l, int b) {
		System.out.println("Area of rectangle is " + (l*b));
	}

	 static void Area(int r) {
		System.out.println("Area of circle : " + (3.1415 * r));
	}
}
