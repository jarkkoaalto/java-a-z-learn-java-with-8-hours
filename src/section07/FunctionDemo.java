package section07;

import java.util.Scanner;

public class FunctionDemo {
	
	public static void main(String [] args) {
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Enter first value :");
		int a = reader.nextInt();
		System.out.println("Enter second value :");
		int b = reader.nextInt();
		sum(a,b);
		
	}
	
	
	public static void sum(int g, int h) {
		
		int sum = g + h;
		System.out.println("Sum is: " + sum);
	}

}
