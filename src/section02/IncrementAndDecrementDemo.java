package section02;

public class IncrementAndDecrementDemo {

	public static void main(String[] args) {
		int n1 = 5;
		int n2 = 10;
		
		int n3 = 4;
		
		int sum = (++n1 + n2) * n3;
		System.out.println(sum); // 64

	}

}
