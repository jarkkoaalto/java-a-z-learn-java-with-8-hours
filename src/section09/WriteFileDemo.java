package section09;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;



public class WriteFileDemo {

	static Scanner reader = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		FileWriter f;
		try {
			f = new FileWriter("test.txt");
			f.write("We are going to become a JAVA programmer");
			f.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Done");
	}

}
