package section07;

import java.util.Scanner;

public class LocalAndGlobalVariables {

	static double e = 0.0001;
	
	public static void main(String[] args) {
		
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter value a : ");
		int a = reader.nextInt();
		System.out.println("Enter value b : ");
		int b = reader.nextInt();
		sum(a,b);
	
		
	}

	public static void sum(int u, int y) {
		int sum = u +y ;
		System.out.println("Sum is : " + sum);
	}
	
	public static void newmain() {
		
	}
}
