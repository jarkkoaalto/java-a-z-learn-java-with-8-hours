package section05;

public class DoWhileDemo {

	public static void main(String[] args) {
		
		int i = 0;
		while(i <= 100) {
			// System.out.println("Jarkko " + i);
			if(i % 2 == 0) {
			System.out.println("Even number are " + i);
			}
			
			if(i % 2 != 0) {
				System.out.println("Odd Number are "+ i);
			}
			i++;
		}
		int u = 0;
		
		
		// do allways print one time
		do {
			System.out.println(" You are good to go! ");
			u++;
		}while(u < 100);
		
	}

}
