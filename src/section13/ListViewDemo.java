package section13;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class ListViewDemo {

	public void start(Stage primaryStage) {
		FlowPane root = new FlowPane();
		root.setOrientation(Orientation.VERTICAL);
		
		Label l = new Label("Select");
		Label la = new Label("You select : ");
		
		root.getChildren().add(l);
		root.getChildren().add(la);
		
		// create list 
		// define set
		
		ObservableList<String> name = FXCollections.observableArrayList("John", "Matt","Ben","Harry","Jack");
		
		ListView<String> lv = new ListView<String>(name);
		lv.setPrefSize(300, 150);
		lv.setOrientation(Orientation.VERTICAL);
	
		// Defining multi select option
		MultipleSelectionModel<String> lvModule = lv.getSelectionModel();
		
		lvModule.selectedItemProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				
				System.out.println("Selected : " + newValue);
				la.setText("Name is : " + newValue);
				l.setText("You have clicked");
				
				
			}
			
			
			
		});
		root.getChildren().add(lv);
		
		
		Scene scene = new Scene(root, 300, 250);
		
		primaryStage.setTitle("List view");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	
	public static void main(String[] args) {
		Application.launch(args);

	}


	

}
