package section12;

import java.util.Iterator;
import java.util.LinkedList;

public class Collection01 {

	public static void main(String[] args) {
		LinkedList<String> ll = new LinkedList<String>();
		ll.add("John");
		ll.add("Cena");
		
		Iterator<String> itr = ll.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}

	}

}
