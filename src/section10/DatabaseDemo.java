package section10;
import java.sql.*;
import java.util.Scanner;
public class DatabaseDemo {

	static Scanner read = new Scanner(System.in);
	public static void main(String[] args) {
		
		Connection c = null;
		System.out.println("Enter id to search for : ");
		int id = read.nextInt();
		
		
		
		
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:admin.db");
			System.out.println("Connection is established");
			
			/*
			String sqladd = "insert into" 
					+ " admins(user_name,password)"
					+"values('jarkko','dfww')";
			Statement stmt = c.createStatement();
			stmt.execute(sqladd);
			c.commit(); // adding, updatind or deleting data have commit that is important
			
			
			// readdata
			String sqlread = "Select * from admins";
			Statement stmt2 = c.createStatement();
			ResultSet rs = stmt2.executeQuery(sqlread);
			
			while(rs.next()) {
				int id1 = rs.getInt("id");
				String user = rs.getString("user_name");
				String passwd = rs.getString("password");
				
				System.out.println(id1 + "\t" + user + "\t" + passwd);
				
			}
			
			*/
			
			String sqldel = "delete * from admins where id = " + id;
			java.sql.Statement stmt = c.createStatement();
			stmt.execute(sqldel);
			
			c.close(); // close allways connection, because another user may want use it
			
			
			
			
			
		}catch(Exception ex) {
			System.out.println("Cannot connect to db " + ex.getMessage());
			System.exit(0);
		}

	}

}
