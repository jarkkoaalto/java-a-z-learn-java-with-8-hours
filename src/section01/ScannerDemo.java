package section01;

import java.util.Scanner;

public class ScannerDemo {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		
		System.out.print("Enter a first number: ");
		int a = reader.nextInt();
	
		System.out.print("Enter a second number: ");
		int b = reader.nextInt();
		
		int sum  =a+b;
		System.out.println("Sum is : " + sum);

	}

}
