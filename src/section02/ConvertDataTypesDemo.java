package section02;

public class ConvertDataTypesDemo {

	public static void main(String[] args) {
		
		int age = 20;
		double salary = 35.5;
		String num = "3";
		
		double newage = (double)age;
		int newnum = Integer.parseInt(num);
		
		System.out.println(newage);
		System.out.println(newnum);

		
	}

}
