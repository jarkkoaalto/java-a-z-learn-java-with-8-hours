package section03;

import java.util.Scanner;

public class IfElseDemo {

	public static void main(String[] args) {
		
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter your marks");
		int mark = reader.nextInt();
		int age = 18;
		if(mark >= 90) {
			// code inside this will run if condition is true
			System.out.println("Very Good");
		}else if(mark < 89 && mark > 50) {
			System.out.println("Good");
		} else {
			System.out.println("Fail");
		}

	}

}
