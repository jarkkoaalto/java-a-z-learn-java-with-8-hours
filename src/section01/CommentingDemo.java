package section01;

public class CommentingDemo {
	public static void main(String []args) {
		int a = 8;
		int b = 4;
		int c = a + b;
		
		System.out.println("Before Commenting");
		System.out.println(c);
		System.out.println("After Commenting");
		// System.out.println(c);
		
		/*
		 * 
		 * This ismultible line comment
		 */
	}
}
