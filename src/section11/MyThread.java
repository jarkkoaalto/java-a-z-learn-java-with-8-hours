package section11;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MyThread extends Thread  {
	
	
	@Override
	public void run() {
		// Whatever code that should run as service int his scope
		int x = 0;
		while(true) {
			System.out.println(x);
			x++;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException ex) {
				Logger.getLogger(MyThread.class.getName()).log(Level.SEVERE, null,ex);
				
			}
		}
	}

}
